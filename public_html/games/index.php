<?php

require_once '../../Games/autoload.php';

use Services\Router;

use Models\Word;
use Models\User;
use Models\PasswordReset;
use Services\Auth;
use Services\Session;
use Services\Password;
use Services\View;

Router::match();
