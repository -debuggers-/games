$(document).ready(function() {
    $(function() {
        $.material.init();
    });

    $('.game-block').hover(
        function() {
            var element = $(this);
            element.find('.fa').removeClass('fa-paw').addClass('fa-gamepad');
            
        },
        function() {
            var element = $(this);
            element.find('.fa').removeClass('fa-gamepad').addClass('fa-paw');
        }
    ).click(
        function() {
            $(this).css('outline', 'none');
        }
    );
});