<?php

$_ENV['DEBUG'] = true;

$_ENV['DB_HOST'] = 'localhost';
$_ENV['DB_NAME'] = 'dbname';
$_ENV['DB_USER'] = 'username';
$_ENV['DB_PASSWORD'] = 'password';
$_ENV['SITE_URL'] = 'http://localhost/';
