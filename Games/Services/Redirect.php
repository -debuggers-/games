<?php

namespace Services;

/**
 * Redirect service
 * 
 */
class Redirect
{
	
	/**
	 * Host name
	 * @var string
	 */
	private $host;


	/**
	 * URI base
	 * @var string
	 */	
	private $base;


	/**
	 * Constructor
	 *
	 * @param string
	 */
	public function __construct($base = '/')
	{		
		$this->host = $_SERVER['HTTP_HOST'];
		$this->base = $base;
	}


	/**
	 * Redirects to specific URI
	 *
	 * @param string
	 */
	public function to($uri)
	{
		header('Location: '. $this->makeUrl($uri));
		die;
	}


	/**
	 * Redirects to base
	 *
	 */
	public function home()
	{
		header('Location: '. $this->makeUrl());
		die;
	}


	/**
	 * Makes URL string
	 *
	 * @param string
	 * @return string
	 */
	private function makeUrl($uri = null)
	{
		// Creates home URL
		$url = $this->host .'/'. $this->base;

		// Checks if specific URI string is required
		if ($uri) {
			$url .= '/'. $uri;
		}

		// Replaces multiple slahses with one
		return 'http://'. preg_replace('/(\/+)/', '/', $url);
	}

}
