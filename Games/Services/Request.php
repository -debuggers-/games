<?php

namespace Services;

/**
 * Request service
 *
 */
class Request
{


    private $input = [];


    /**
     * Constructor
     *
     */
    public function __construct()
    {
        $this->input = array_merge($_POST, $_GET);
    }


    /**
     * Gets request input variable
     *
     */
    public function __get($key)
    {
        return (array_key_exists($key, $this->input))
            ? $this->input[$key]
            : null;
    }


    /**
     * Gets all input variables
     *
     */
    public function all()
    {
        return $this->input;
    }


    /**
     * Gets specific input variables
     *
     * @param array
     * @return array
     */
    public function only(array $keys)
    {
        return array_intersect_key($this->input, array_flip($keys));
    }


    /**
     * Gets or sets cookie variable
     *
     */
    public function cookie($key, $value = null)
    {
        if (func_num_args() == 1) {
            // Returns cookie value if exists
            return array_key_exists($key, $_COOKIE)
                ? $_COOKIE[$name]
                : null;    
        }
        
        // Sets cookie varable
        setcookie($key, $value, time()+3600, '', '', true);
    }


}
