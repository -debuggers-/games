<?php

namespace Services;

/**
 * Controller
 *
 */
class Controller
{

    /**
     * Request service instace
     * @var Services/Request
     */
    protected $request;


    /**
     * View service instace
     * @var Services/View
     */
    protected $view;


    /**
     * Redirect service instace
     * @var Services/Redirect
     */
    protected $redirect;


    /**
     * Constructor
     *
     */
    public function __construct()
    {
        $this->request = new Request;
        $this->view = new View;
        $this->redirect = new Redirect('/games');
    }

}
