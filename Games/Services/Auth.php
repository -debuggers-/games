<?php

namespace Services;

use Models\User;
use Models\Game;

/**
 * Authentication
 *
 */
class Auth
{

    /**
     * Instance of user model
     * @var Models\User
     */
    private static $user = null;


    /**
     * Auth session variable name
     * @var string
     */
    private static $name = '_auth';


    /**
     * Attempts to login user
     *
     * @param string
     * @param string
     * @return bool
     */
    public static function login($email, $password)
    {
        // Checks if email and password is not empty
        if (empty($email) || empty($password)) {
            return false;
        }

        // Tries to find user by email address
        $user = (new User)
            ->where('email', $email)
            ->first();

        // Cheks if user exists and verifies password
        if ($user && Password::verify($password, $user->password)) {
            // Creates user session
            Session::put(static::$name, $user->id);

            // Logged in successfully
            return true;
        }
        
        // Login failed
        return false;
    }


    /**
     * Checks if user is authenticated
     *
     * @return bool
     */
    public static function check()
    {
        return Session::has(static::$name);
    }


    /**
     * Logs out the user
     *
     */
    public static function logout()
    {
        // Destroys user model instance
        static::$user = null;

        // Clears session variable
        Session::forget(static::$name);

        // Regenerates session id
        Session::regenerate();
    }


    /**
     * Gets the model of authenticated user
     *
     * @return Models\User
     */
    public static function user()
    {
        // Checks if user is logged
        if ( ! static::check()) {
            return;
        }

        // Checks if user model already created
        if ( ! (static::$user instanceof User)) {
            // Creates user model
            static::$user = (new User)
                ->find(Session::get(static::$name));
        }

        // Returns user model
        return static::$user;
    }


    /**
     * Authenticates game by API key
     *
     * @param string
     * @return bool
     */
    public static function game($key)
    {
        // Tries to find the game by API key
        $game = (new Game)
            ->where('api_key', $key)
            ->first();

        return ! is_null($game);
    }

}
