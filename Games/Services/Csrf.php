<?php

namespace Services;

/**
 * CSRF service
 *
 */
class Csrf
{

    /**
     * CSRF variable name for session
     * @var string
     */
    private static $name = '_csrf';
    
    
    /**
     * Random CSRF token
     * @var string
     */
    private static $token = null;


    /**
     * CSRF token creation time
     * @var integer
     */
    private static $time = null;


    /**
     * CSRF token lifetime in seconds
     * @var integer
     */
    private static $lifetime = 7200;


    /**
     * Initializes CSRF service
     *
     */
    public static function init()
    {
        // Checks if token is stored in session
        if (Session::has(self::$name)) {
            // Retrieves CSRF token from session
            self::$token = Session::get(self::$name)['token'];
            self::$time = Session::get(self::$name)['time'];

            // Checks if token expired
            if (self::expired()) {
                self::generate();
            }
        } else {
            self::generate();
        }
    }


    /**
     * Returns CSRF token value
     *
     * @return string
     */
    public static function token()
    {
        return self::$token;
    }


    /**
     * Verifies token
     *
     * @param string
     */
    public static function verify($token)
    {
        // Checks if tokens match
        if (self::$token !== $token) {
            throw new \Exception('Token verify error.');
        }
    }


    /**
     * Generates CSRF token
     *
     */
    private static function generate()
    {
        // Generates new random token
        self::$token = bin2hex(openssl_random_pseudo_bytes(32));
        self::$time = time();

        // Stores token in session
        Session::put(self::$name, [
            'token' => self::$token,
            'time' => self::$time
        ]);
    }


    /**
     * Checks if token has expired
     *
     */
    private static function expired()
    {
        return (self::$time + self::$lifetime < time());
    }

}
