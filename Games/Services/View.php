<?php

namespace Services;

/**
 * View
 *
 */
class View
{

    /**
     * View file directory
     * @var string
     */
    private $dir = '../../Games/Views/';


    /**
     * Default layout view name
     * @var string
     */
    private $layout = 'shared/main';


    /**
     * View data
     * @var array
     */
    private $data = [];


    /**
     * Renders view with layout
     *
     * @param string
     * @param array
     * @return string
     */
    public function render($name, array $data = [])
    {
        // Prepares file paths
        $layout = $this->dir . $this->layout .'.php';
        $view = $this->dir . $name .'.php';

        // Merges view data variables
        $this->data = array_merge($this->data, $data);

        // Gets view output
        $this->content = $this->getOutput($view);

        // Returns layout output
        return $this->getOutput($layout);
    }


    /**
     * Sets view variable 
     *
     * @param string
     * @param mixed
     */
    public function __set($key, $value)
    {
        $this->data[$key] = $value;
    }


    /**
     * Get view variable 
     *
     * @param string
     * @return mixed
     */
    public function __get($key)
    {
        return array_key_exists($key, $this->data)
            ? $this->data[$key]
            : null;
    }


    /**
     * Sets custom layout file
     *
     * @param string
     * @return $this
     */
    public function layout($name)
    {
        $this->layout = $name;
        return $this;
    }


    /**
     * Gets file output
     * 
     * @param string
     * @return string
     */
    private function getOutput($file)
    {
        // Checks if view file exists
        if ( ! file_exists($file)) {
            throw new \Exception('View file does not exist.');
        }

        // Extracts view data variables
        extract($this->data);
        
        // Starts output buffering
        ob_start();

        // Includes view file
        include $file;

        // Returns view output as string
        return ob_get_clean();
    }

}
