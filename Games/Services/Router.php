<?php

namespace Services;

/**
 * Router
 *
 */
class Router
{
    private static $routes = array();
    private static $controllers = array();
    private static $methods = array();
    private static $url;

    # map possible Urls to appropriate dispatchers
    public static function map($array)
    {
        foreach ($array as $route => $dispatcher)
        {
            self::$routes[] = $route;

            if (is_array($dispatcher))
            {
                self::$routes[] = $route;

                $dispatcher['GET'] = explode('@', $dispatcher['GET']);
                self::$controllers[] = array_shift($dispatcher['GET']);
                self::$methods[] = array_shift($dispatcher['GET']);

                $dispatcher['POST'] = explode('@', $dispatcher['POST']);
                self::$controllers[] = array_shift($dispatcher['POST']);
                self::$methods[] = array_shift($dispatcher['POST']);
            }
            else
            {
                $dispatcher = explode('@', $dispatcher);
                self::$controllers[] = array_shift($dispatcher);
                self::$methods[] = array_shift($dispatcher);
            }
        }
    }

    # parse Url request data
    public static function parse()
    {
        # clean Url data
        self::$url = rtrim(preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']), '/');
        self::$url = filter_var(self::$url, FILTER_SANITIZE_URL);
        self::$url = rawurldecode(self::$url);

        # return request type
        return $_SERVER['REQUEST_METHOD'];
    }

    # extract array of Url parameters using given route pattern
    public static function extract($route)
    {
        # find all {params} & store them in $matches array
        preg_match_all('#{\w+}#', $route, $matches);
        $matches = array_shift($matches);

        # use $parameters to store actual extracted {param} values
        $parameters = array();

        foreach ($matches as $match)
        {
            # use each unique route pattern's {param} as delimiter
            $main = explode($match, $route);

            # get what's to the left & to the right from route pattern's {param}
            $left = array_shift($main);
            $right = array_shift($main);

            # convert both extracted route pattern's sides for replacement
            $leftPattern = '#' . preg_replace('#{\w+}#', '.+', $left) . '#';
            $rightPattern = '#' . preg_replace('#{\w+}#', '.+', $right) . '#';

            # extract actual {param} value by removing unnecessary url parts
            $parameter = preg_replace($leftPattern, '', self::$url);
            $parameter =  preg_replace($rightPattern, '', $parameter);

            # store current parameter
            $parameters[] = $parameter;

            # SUPERB IMPORTANT:
            # replace current route pattern's {param} with its actual extracted url value
            $route = str_replace($match, $parameter, $route);
        }

        return $parameters;
    }

    # match current Url to appropriate route pattern
    public static function match()
    {
        # clean Url data & return request type
        $type = self::parse();

        $i = 0;
        foreach (self::$routes as $route)
        {
            $pattern = '#^' . preg_replace('#{\w+}#', '.+', $route) . '$#';

            if (preg_match($pattern, self::$url))
            {
                # help find appropriate controller & method for GET or POST
                if ($type === 'POST') ++$i;

                # set appropriate controller & method
                $controller = new self::$controllers[$i];
                $method = self::$methods[$i];

                # extract Url parameters using current route pattern
                $parameters = self::extract($route);

                # dispatch conroller, method & parameters
                $result = call_user_func_array([$controller, $method], $parameters);

                # checks if result is scalar
                if (is_scalar($result)) {
                    echo $result;
                }

                # exit foreach loop
                return true;
            }

            $i++;
        }

        # Page not found
        http_response_code(404);
        echo 'Page not found!';
    }
}
