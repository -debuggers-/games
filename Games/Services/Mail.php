<?php

namespace Services;

/**
 * Mail service
 * 
 */
class Mail
{

    /**
     * Sends email
     *
     * @param string
     * @return bool
     */
    public static function send($message, $to, $subject)
    {
        return mail($to, $subject, $message);
    }

}
