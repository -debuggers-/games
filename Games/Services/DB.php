<?php

namespace Services;

use PDO;

/**
 * Database connection class
 *
 */
class DB
{

    /**
     * PDO database connection
     * @var object
     */
    private static $connection;


    /**
     * Creates PDO instance and connects database
     *
     * @return PDO
     */
    public static function connect()
    {
        // Checks if already connected
        if ( ! isset(self::$connection))
        {
            self::$connection = new PDO('mysql:host='. DB_HOST .';dbname='. DB_NAME .';charset=utf8', DB_USER, DB_PASSWORD);
            self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$connection;
    }


    private function __construct() {}
    private function __clone() {}

}
