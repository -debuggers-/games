<?php

namespace Services;

use Models\User;
use Models\PasswordReset;

/**
 * Password service
 * 
 */
class Password
{

   /**
     * Cost factor
     * @var integer
     */
    private static $cost = 12;


    /**
     * Hashes user's password
     *
     * @param string
     * @return string|bool
     */
    public static function hash($password)
    {
        return password_hash($password, PASSWORD_DEFAULT, [
            'cost' => static::$cost
        ]);
    }


    /**
     * Verifies user's password
     *
     * @param string
     * @param string
     * @return bool
     */
    public static function verify($password, $hash)
    {
        return password_verify($password, $hash);
    }


    /**
     * Validates password
     *
     * @param string
     * @param string
     * @return bool
     */
    public static function validate($password, $password_confirm)
    {
        // Checks if password matches confirmation
        return (mb_strlen($password) >= 6 && $password === $password_confirm);
    }

}
