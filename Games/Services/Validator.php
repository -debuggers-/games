<?php

namespace Services;

/**
 * Validator
 * 
 */
class Validator
{

    /**
     * Data to be validated
     * @var array
     */
    private $data = [];
    

    /**
     * Field rules
     * @var array
     */    
    private $rules = [];


    /**
     * Error messages
     * @var array
     */
    public $errors = [];


    /**
     * Constructor
     *
     * @param array
     * @param array
     */
    public function __construct(array $data, array $rules)
    {
        $this->data = $data;
        $this->rules = $rules;
    }


    /**
     * Gets error list
     *
     * @return array
     */
    public function errors()
    {
        return $this->errors;
    }


    /**
     * Checks if validation failed
     *
     * @return bool
     */
    public function failed()
    {
        // Runs validation        
        $this->validate();

        // Checks if any error occurred
        return ! empty($this->errors);
    }


    /**
     * Checks if validation passed
     *
     * @return bool
     */
    public function passed()
    {
        // Runs validation
        $this->validate();

        // Checks if any error occurred
        return empty($this->errors);
    }


    /**
     * Checks if field exists and it's value is not empty
     *
     * @param string     
     * @return bool     
     */
    public function required($field)
    {
        if ( ! $this->getValue($field)) {
            $this->setError($field, 'Lauks jāaizpilda obligāti.');
            return false;
        }
        return true;
    }


    /**
     * Validates email address
     *
     * @param string
     * @return bool
     */
    public function email($field)
    {
        if ( ! filter_var($this->getValue($field), FILTER_VALIDATE_EMAIL)) {
            $this->setError($field, 'Nepareizs e-pasta adreses formāts.');
            return false;
        }
        return true;
    }


    /**
     * Validates minimum length of the string
     *
     * @param string
     * @param integer
     * @return bool
     */
    public function min($field, $length)
    {
        if (mb_strlen($this->getValue($field)) < intval($length)) {
            $this->setError($field, 'Minimālais garums ir '.intval($length).' simboli.');
            return false;
        }
        return true;
    }


    /**
     * Validates maximum length of the string
     *
     * @param string
     * @param integer
     * @return bool
     */
    public function max($field, $length)
    {
        if (mb_strlen($this->getValue($field)) > intval($length)) {
            $this->setError($field, 'Maksimālais garums ir '.intval($length).' simboli.');
            return false;
        }     
        return true;
    }


    /**
     * Checks if field value is unique according to the model
     *
     * @param string
     * @param string
     * @return bool
     */
    public function unique($field, $model)
    {
        if ((new $model())->where($field, $this->getValue($field))->first()) {
            $this->setError($field, 'Lauka vērtība jau ir aizņemta!');
            return false;
        }
        return true;
    }


    /**
     * Confirms field
     *
     * @param string
     * @param string
     * @return bool     
     */
    public function confirm($field, $confirmation_field)
    {
        if ($this->getValue($field) !== $this->getValue($confirmation_field)) {
            $this->setError($field, 'Ievadītajiem laukiem jābūt vienādiem.');
            return false;
        }
        return true;
    }


    /**
     * Performs data validation
     *
     */
    private function validate()
    {
        // Goes through all fields
        foreach ($this->rules as $field => $rules) {
            // Checks if field is optional
            if ( ! $this->getValue($field) && reset($rules) != 'required') {
                continue;
            }
            // Goes through all field rules
            foreach ($rules as $rule) {
                // Checks rule
                if ( ! $this->checkRule($field, $rule)) {
                    break;
                }
            }
        }
    }


    /**
     * Checks rule for the field
     *
     * @param string
     * @param string
     * @return bool
     */
    private function checkRule($field, $rule)
    {
        $params = [$field];
        $method = $rule;

        // Tries to explode rule string
        $segments = explode(':', $rule);

        // Checks if there is any prameter for the rule
        if (count($segments) == 2) {
            $params = array_merge($params, explode(',', $segments[1]));
            $method = $segments[0];
        }

        return call_user_func_array([$this, $method], $params);
    }


    /**
     * Gets field value
     *
     * @param string
     * @return mixed
     */
    private function getValue($field)
    {
        return array_key_exists($field, $this->data)
            ? $this->data[$field]
            : null;
    }


    /**
     * Sets error message for the field
     *
     * @param string
     * @param string
     */
    private function setError($field, $message)
    {
        $this->errors[$field] = $message; 
    }

}
