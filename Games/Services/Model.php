<?php

namespace Services;

use Services\DB;
use PDO, PDOStatement;

/**
 * Model class
 *
 */
class Model
{

    /**
     * PDO database connection instance
     * @var PDO
     */
    protected $db;


    /**
     * Array of model attributes
     * @var array
     */    
    protected $attributes = [];


    /**
     * Indicates if models exists in database
     * @var bool
     */    
    protected $exists = false;


    /**
     * List of selected columns
     * @var array
     */
    protected $columns = ['*'];


    /**
     * Query ordering
     * @var array
     */
    protected $order;


    /**
     * Limit value
     * @var integer
     */
    protected $limit = null;


    /**
     * Offset value
     * @var integer
     */
    protected $offset = null;


    /**
     * List of SQL where statements
     * @var array
     */
    protected $where = [];


    /**
     * List of binding parameters
     * @var array
     */
    protected $params = [];


    /**
     * Constructor
     *
     * @param array
     * @param bool
     */
    public function __construct(array $attributes = [], $exists = false)
    {
        $this->db = DB::connect();
        $this->exists = (bool) $exists;
        $this->fill($attributes);
    }


    /**
     * Gets model attribute
     *
     * @param string
     * @return mixed
     */
    public function __get($name)
    {
        return array_key_exists($name, $this->attributes)
            ? $this->attributes[$name]
            : null;
    }


    /**
     * Sets model attribute
     *
     * @param string
     */
    public function __set($name, $value)
    {
        // Forbids to change model ID manually
        if ($name == 'id') {
            return;
        }

        // Sets model attribute
        $this->attributes[$name] = $value;
    }


    /**
     * Handles prepared statement
     *
     * @param string
     * @param array
     * @return $this
     */
    public function query($sql, array $params = [])
    {
        // Prepares and executes SQL query with parameters
        $stmt = $this->db->prepare($sql);

        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute($params);

        // Returns PDOStatement
        return $stmt;
    }


    /**
     * Gets the first row as a model object
     *
     * @return mixed
     */
    public function first()
    {
        $this->limit = 1;

        // Fetches first row from database
        $row = $this
            ->query($this->sql('select'), $this->params)
            ->fetch();

        // Checks if any row was found
        if ($row) {
            return new static($row, true);
        }

        // Nothing was found
        return null;
    }


    /**
     * Gets all rows as array of model objects
     *
     * @return array
     */
    public function get()
    {
        // Fetches all rows from database
        $rows = $this
            ->query($this->sql('select'), $this->params)
            ->fetchAll();

        $models = [];
        // Creates model object for each row
        foreach ($rows as $row) {
            $models[] = new static($row, true);
        }

        // Returns array of model objects
        return $models;
    }


    /**
     * Gets first row by id
     *
     * @param integer
     * @return Services\Model
     */
    public function find($id)
    {
        return $this
            ->select('*')
            ->where('id', '=', intval($id))
            ->first();
    }


    /**
     * Sets selected columns
     *
     * @param array $columns
     * @return $this
     */    
    public function select()
    {
        if (func_num_args() > 0) {
            $this->columns = func_get_args();

            // Ensures that id column is selected anyway
            if ( ! in_array('id', $this->columns) && ! in_array('*', $this->columns)) {
                $this->columns[] = 'id';
            }
        }

        // Returns current object
        return $this;
    }


    /**
     * Adds where statement
     *
     * @param string|array
     * @param string
     * @param string
     * @return $this
     */
    public function where($column, $operator, $value = null)
    {
        // Sets '=' as a default operator, if only two parameters passed
        if (func_num_args() == 2) {
            $operator = '=';
            $value = func_get_arg(1);
        }

        $this->params[] = $value;
        $this->where[] = "`{$column}` {$operator} ?";
        
        // Returns current object
        return $this;
    }


    /**
     * Sets column order
     *
     * @return $this
     */
    public function orderBy($column, $direction = 'ASC')
    {
        // Turns to uppercase
        $direction = strtoupper($direction);

        // Checks if order direction is valid
        if ( ! in_array($direction, ['DESC', 'ASC'])) {
            throw new \Exception('Invalid order direction.');
        }

        // Sets order
        $this->order = "`". $column ."` ". $direction;

        // Returns current object
        return $this;
    }


    /**
     * Sets the number of rows to take
     *
     * @param integer
     * @return $this
     */
    public function take($limit)
    {
        // Sets limit
        $this->limit = intval($limit);

        // Returns current object
        return $this;
    }


    /**
     * Deletes current model
     *
     * @return bool
     */
    public function delete()
    {
        // Checks if model exist
        if ($this->id) {
            $this->where('id', '=', $this->id);
        }

        // Counts affected rows
        $affected = $this
            ->query($this->sql('delete'), $this->params)
            ->rowCount();

        // Checks if any row affected
        if ($affected > 0) {
            // Clears model attributes
            $this->attributes = [];
            $this->exists = false;

            // Model deleted successfully
            return true;
        }

        // Nothing has deleted
        return false;
    }


    /**
     * Saves current model to the database
     *
     * @return integer|bool
     */
    public function save()
    {
        // Checks if model is empty
        if (empty($this->attributes)) {
            throw new \Exception('Cannot save empty model.');
        }

        if ($this->id) {
            // Updates existing model
            return $this->update($this->attributes);
        }

        // Inserts new model
        if ($this->insert()) {
            $this->exists = true;
            return $this->attributes['id'] = $this->db->lastInsertId();
        }

        // Nothing has updated
        return false;
    }


    /**
     * Updates existing model
     *
     * @param array
     * @return bool
     */
    public function update(array $options)
    {
        // Checks if model exist
        if ($this->id) {
            $this->where('id', '=', $this->id);
        }

        // Counts affected rows
        $affected = $this
            ->query($this->sql('update', array_keys($options)), array_merge(array_values($options), $this->params))
            ->rowCount();

        if ($affected > 0) {
            // Updates model attributes
            $this->attributes = array_merge($this->attributes, $options);
            return true;
        }

        // Nothing has updated
        return false;
    }


    /**
     * Inserts new model into database
     *
     * @param array
     * @return bool
     */
    private function insert()
    {
        // Executes query
        return $this
            ->query($this->sql('insert', array_keys($this->attributes)), array_values($this->attributes))
            ->rowCount() > 0;
    }


    /**
     * Creates SQL query string
     *
     * @param string
     * @param array
     * @return string
     */
    public function sql($statement, array $columns = [])
    {
        # must be set as private function, but later
        // Sets SQL statement
        switch ($statement) {
            case 'select':
                $sql = "SELECT ". implode(', ', $this->columns) ." FROM `{$this->table}`";
            break;
            case 'delete':
                $sql = "DELETE FROM `{$this->table}`";
            break;
            case 'update':
                $sql = "UPDATE `{$this->table}` SET ". implode(', ', array_map(function($column) {
                    return "`". $column ."` = ?";
                }, $columns));
            break;
            case 'insert':
                $markers = implode(', ', array_fill(0, count($columns), '?'));
                $sql = "INSERT INTO `{$this->table}` (". implode(', ', $columns) .") VALUES ({$markers})";            
            break;
            default:
                throw new \Exception('Wrong SQL statement.');
        }

        // Sets where statements
        if ( ! empty($this->where)) {
            $sql .= " WHERE ". implode(' AND ', $this->where);
        }
        // Sets ordering
        if ($this->order) {
            $sql .= " ORDER BY {$this->order}";
        }
        // Sets limit
        if ($this->limit) {
            $sql .= " LIMIT {$this->limit}";
        }

        // Returns SQL query
        return $sql;
    }


    /**
     * Fills model with attributes
     *
     * @param array
     */
    private function fill(array $attributes)
    {
        foreach ($attributes as $name => $value) {
            // Forbids to fill ID attribute
            if ( ! $this->exists && $name == 'id') {
               continue;
            }
            $this->attributes[$name] = $value;
        }   
    }

}
