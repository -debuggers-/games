<?php

namespace Services;

/**
 * Session service
 *
 */
class Session
{

    /**
     * Initializes session
     *
     */
    public static function init()
    {
        // Checks if session has not started yet
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        // Handles flash variables
        self::handleFlash();
    }


    /**
     * Returns session id
     * 
     * @return string
     */
    public static function id()
    {
        return session_id();
    }


    /**
     * Gets session variable by key
     *
     * @return mixed
     */
    public static function get($key)
    {
        self::check();

        return array_key_exists($key, $_SESSION)
            ? $_SESSION[$key]
            : null;
    }


    /**
     * Sets flash variable (remains only per one request)
     *
     * @param string
     * @param mixed
     */
    public static function flash($key, $value)
    {
        self::check();

        // Adds flash variable to the session
        self::put($key, $value);

        // Sets flash variable as new
        $_SESSION['_new'][] = $key;
    }


    /**
     * Checks if session variable exists
     *
     * @return bool
     */
    public static function has($key)
    {
        self::check();

        return array_key_exists($key, $_SESSION);
    }


    /**
     * Gets all sessions variables
     *
     * @return mixed
     */
    public static function all()
    {
        self::check();

        return $_SESSION;
    }


    /**
     * Stores or rewrites session variable
     *
     * @param string|array
     * @param mixed
     */
    public static function put($key, $value = null)
    {
        self::check();

        // Checks if first parameter is array
        if (is_array($key)) {
            // Stores multiple variables
            foreach ($key as $k => $value) {
                self::put($k, $value);
            }
            return;
        }

        $_SESSION[$key] = $value;
    }


    /**
     * Deletes session variable
     *
     * @param string
     * @param mixed
     */
    public static function forget($key)
    {
        self::check();

        unset($_SESSION[$key]);
    }


    /**
     * Regenerates session id
     *
     * @return bool
     */
    public static function regenerate()
    {
        return session_regenerate_id();
    }


    /**
     * Destroys all data registered to a session
     *
     * @return bool
     */
    public static function destroy()
    {
        self::check();

        return session_destroy();
    }


    /**
     * Clears all session variables
     *
     */
    public static function clear()
    {
        session_unset();
    }


    /**
     * Checks if session is initialized
     *
     */
    private static function check()
    {
        // Checks session status
        if (session_status() == PHP_SESSION_NONE) {
            throw new \Exception('Session is not initialized.');
        }
    }


    /**
     * Handles flash variables
     *
     */
    private static function handleFlash()
    {
        if (isset($_SESSION['_old'])) {
            // Removes all old flash variables
            foreach ($_SESSION['_old'] as $key) {
                self::forget($key);
            }
            unset($_SESSION['_old']);
        }

        if (isset($_SESSION['_new'])) {
            // Matches new flash variables as old
            $_SESSION['_old'] = $_SESSION['_new'];
            unset($_SESSION['_new']);            
        }
    }

}
