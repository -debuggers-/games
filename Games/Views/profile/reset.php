<div class="row">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">

        <?php if (Services\Session::has('ok')): ?>
            <p>Parole tika veiksmīgi atjaunota!</p>

        <?php else: ?>
            <h3>Paroles maiņa</h3>

            <form action="" method="post" class="form-horizontal">
                <input type="hidden" name="csrf_token" value="<?php echo $csrf_token ?>">

                <?php if( ! empty($error)): ?>
                    <p class="text-danger"><?php echo $error ?></p>
                <?php endif ?>

                <div class="form-group"> 
                    <input type="password" name="password" class="form-control" placeholder="Parole">
                </div>

                <div class="form-group">
                    <input type="password" name="password_confirm" class="form-control" placeholder="Parole vēlreiz">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-raised btn-success btn-block ripple-effect">Atjaunot paroli</button>
                </div>
            </form>

        <?php endif ?>
    </div>
</div>