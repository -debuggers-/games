<div class="row">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">

        <?php if(Services\Session::has('ok')): ?>
            <p>Profils tika veiksmīgi izveidots!</p>

        <?php else: ?>
            <h3>Reģistrācija</h3>

            <form action="" method="post" class="form-horizontal">
                <input type="hidden" name="csrf_token" value="<?php echo $csrf_token ?>">

                <div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder="E-pasts" value="<?php if(isset($input['email'])): echo $input['email']; endif ?>">
                    <?php if(isset($errors['email'])): ?><span class="text-danger"><?php echo $errors['email'] ?></span><?php endif ?>
                </div>

                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="Tavs vārds" value="<?php if(isset($input['name'])): echo $input['name']; endif ?>">
                    <?php if(isset($errors['name'])): ?><span class="text-danger"><?php echo $errors['name'] ?></span><?php endif ?>
                </div>

                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Parole">
                    <?php if(isset($errors['password'])): ?><span class="text-danger"><?php echo $errors['password'] ?></span><?php endif ?>
                </div>

                <div class="form-group">
                    <input type="password" name="password_confirm" class="form-control" placeholder="Parole vēlreiz">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-raised btn-success btn-block ripple-effect">Reģistrēties</button>
                </div>
            </form>
            
        <?php endif ?>
        
    </div>
</div>