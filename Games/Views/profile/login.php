<h3 class="text-center">
    Šajā lapā vari gan autorizēties, gan <a id="signup" href="/games/profile/signup">reģistrēties</a>
</h3>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
        <form action="" method="post" class="form-horizontal">
            <input type="hidden" name="csrf_token" value="<?php echo $csrf_token ?>">

            <?php if( ! empty($error)): ?>
                <p class="text-danger"><?php echo $error ?></p>
            <?php endif ?>

            <div class="form-group"> 
                <input type="text" name="email" class="form-control" placeholder="E-pasts">
            </div>

            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Parole">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-raised btn-success btn-block ripple-effect">Autorizēties</button>
                <a id="forgot" href="/games/profile/forgot" class="btn btn-raised btn-link btn-block ripple-effect"><i class="fa fa-laptop"></i>&nbsp;Aizmirsu paroli</a>
            </div>
        </form>
    </div>
</div>