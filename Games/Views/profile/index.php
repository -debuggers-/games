<div class="row">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">

		<?php if($user): ?>
		    <p>Sveiks, <strong><?php echo $user->name ?>!</strong></p>
		    <p><a href="/games/profile/logout" class="btn btn-raised btn-success ripple-effect">Iziet</a></p>
		    
		<?php else: ?>
			<p>Autorizējies, lai apskatītu profilu!</p>

		<?php endif ?>

    </div>
</div>