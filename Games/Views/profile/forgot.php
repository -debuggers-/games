<div class="row">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">

        <?php if(Services\Session::has('ok')): ?>
            <p>Paroles atjaunošanas links tika nosūtīts uz Jūsu e-pasta adresi!</p>

        <?php else: ?>
            <h3>Paroles atjaunošana caur e-pastu</h3>
            
            <form action="" method="post" class="form-horizontal">
            	<input type="hidden" name="csrf_token" value="<?php echo $csrf_token ?>">

            	<?php if( ! empty($error)): ?>
        			<p class="text-danger"><?php echo $error ?></p>
    			<?php endif ?>

    			<div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder="E-pasts">
                </div>

                <button type="submit" class="btn btn-raised btn-success btn-block ripple-effect">Nosūtīt</button>
            </form>

        <?php endif ?>

    </div>
</div>