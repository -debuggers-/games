<?php
    use Services\Auth;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?php echo $title ?> | debuggers.lv</title>
    <link rel="stylesheet" href="/games/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/games/css/font-awesome-4.5.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/games/css/bootstrap-material-design.min.css" />
    <link rel="stylesheet" href="/games/css/ripples.min.css" />
    <link rel="stylesheet" href="/games/css/style.css" />
</head>
<body>
    <div class="wrapper">
        <div id="header" class="navbar navbar-default">
            <div class="container-fluid">
                <div class="header-row">

                    <!-- xs header -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="pull-left visible-xs">
                            <span>Sadarbībā ar&nbsp;</span>
                            <img src="/games/images/rta-logo.png" alt="Rēzeknes Tehnoloģiju akadēmija" class="logo" />
                        </div>
                    </div>
                    <!-- xs header end -->

                    <!-- sm, md, lg header -->
                    <div class="navbar-collapse collapse navbar-responsive-collapse">
                        <ul class="nav navbar-nav">
                            <li class="visible-xs">
                                <a href="/games/">Sākums</a>
                            </li>
                            <li role="separator" class="divider visible-xs"></li>

                            <?php if (!Auth::user()): ?>
                                <li>
                                    <a href="/games/profile/login">
                                        <i class="fa fa-sign-in"></i></i>&nbsp;Autorizēties
                                    </a>
                                </li>
                                <li role="separator" class="divider visible-xs"></li>
                                <li>
                                    <a href="/games/profile/signup">
                                        <i class="fa fa-user-plus"></i>&nbsp;Reģistrēties
                                    </a>
                                </li>
                                <li role="separator" class="divider visible-xs"></li>

                            <?php else: ?>
                                <li>
                                    <a href="/games/profile">
                                        <i class="fa fa-user"></i>&nbsp;Profils
                                    </a>
                                </li>
                                <li role="separator" class="divider visible-xs"></li>
                                <li>
                                    <a href="/games/profile/logout">
                                        <i class="fa fa-sign-out"></i>&nbsp;Iziet
                                    </a>
                                </li>
                                <li role="separator" class="divider visible-xs"></li>
                            <?php endif ?>
                        </ul>

                        <div id="home" class="col-xs-2 text-center hidden-xs">
                            <a href="/games/" class="btn btn-fab btn-raised btn-primary ripple-effect">
                                <i class="fa fa-home material-icons"></i>
                            </a>
                        </div>

                        <div class="pull-right hidden-xs">
                            <span>Sadarbībā ar&nbsp;</span>
                            <img src="/games/images/rta-logo.png" alt="Rēzeknes Tehnoloģiju akadēmija" class="logo" />
                        </div>

                    </div>
                    <!-- sm, md, lg header end-->

                </div>
            </div>
        </div>

        <div class="bs-component text-center">
            <div class="jumbotron">
                <?php echo $content ?>
            </div>
        </div>
    </div>

    <script src="/games/js/jquery-1.12.2.js"></script>
    <script src="/games/js/bootstrap.min.js"></script>
    <script src="/games/js/material.min.js"></script>
    <script src="/games/js/ripples.min.js"></script>
    <script src="/games/js/script.js"></script>
</body>
</html>
