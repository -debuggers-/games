<div class="row">
    <?php foreach($games as $game): ?>
    <div class="col-xs-12 col-sm-3 col-md-2">
        <a id="game-1" class="game-block" href="/games/play/<?php echo $game->name ?>">
            <i class="fa fa-paw fa-3x"></i>
            <span class="title"><?php echo $game->title ?></span>
        </a>
    </div>
    <?php endforeach ?>
</div>
