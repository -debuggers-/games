<?php

# define autoload function for classes
spl_autoload_register(
    function($class)
    { 
        require_once str_replace('\\', '/', $class) . '.php';
    }
);

# system bootstrap
require_once 'env.php';
require_once 'config.php';
require_once 'routes.php';