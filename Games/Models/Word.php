<?php

namespace Models;

use Services\Model;

/**
 * Word
 *
 */
class Word extends Model
{

    /**
     * Model table name
     * @var string
     */
    protected $table = 'words';


    /**
     * Word type identifiers in database
     * @var array
     */
    private static $types  = [
        'fake' => 'Viltvārds',
        'noun' => 'Lietvārds',
        'verb' => 'Darbības vārds',
        'adjective' => 'Īpašības vārds',
        'adverb' => 'Apstākļa vārds',
        'pronoun' => 'Vietniekvārds',
        'numeral' => 'Skaitļa vārds'
    ];


    /**
     * Word limit value
     * @var array
     */
    protected $limit = 100;


    /**
     * Word order type
     * @var string
     */ 
    protected $order = 'RAND()';


    /**
     * Sets word types
     *
     * @param string
     * @return $this
     */
    public function type($types)
    {
        // Gets list of valid types
        if ( ! is_array($types)) {
            $types = func_get_args();
        }

        if ( ! empty($types)) {
            foreach ($types as $type) {
                // Checks if valid type were passed
                if ( ! array_key_exists($type, self::$types)) {
                    throw new \Exception('Invalid word type.');
                }
                $this->params[] = self::$types[$type];
            }

            // Adds WHERE statement
            $this->where[] = "`type` IN (". implode(', ', array_fill(0, count($types), '?')) .")";            
        }

        // Returns current object
        return $this;
    }


    /**
     * Selects words by specific patterns
     *
     * @param array|string
     * @return $this
     */
    public function match($patterns)
    {
        // Allows to pass patterns both as array and multiple arguments
        if (is_string($patterns)) {
            $patterns = func_get_args();
        }

        $statements = [];

        // Adds like statements
        foreach ($patterns as $pattern) {
            $statements[] = "`word` LIKE BINARY ?";
            $this->params[] = $pattern;
        }

        if ( ! empty($statements)) {
            $this->where[] = "(". implode(' OR ', $statements) .")";
        }

        // Returns current object
        return $this;
    }


    /**
     * Selects words by specific prefix
     *
     * @param array|string
     * @return $this
     */
    public function prefix($patterns)
    {
        if ( ! is_array($patterns)) {
            $patterns = func_get_args();
        }

        array_walk($patterns, function (&$pattern) {
            $pattern .= "%";
        });

        // Matches patterns and returns current object
        return $this->match($patterns);
    }


    /**
     * Selects words by specific suffix
     *
     * @param array|string
     * @return $this
     */
    public function suffix($patterns)
    {
        if (is_string($patterns)) {
            $patterns = func_get_args();
        }

        array_walk($patterns, function (&$pattern) {
            $pattern = "%". $pattern;
        });

        // Matches patterns and returns current object
        return $this->match($patterns);
    }


    /**
     * Selects words which contain specific fragment
     *
     * @param string
     * @return $this
     */
    public function contains($patterns)
    {
        if (is_string($patterns)) {
            $patterns = func_get_args();
        }

        array_walk($patterns, function (&$pattern) {
            $pattern = "%". $pattern ."%";
        });

        // Matches patterns and returns current object
        return $this->match($patterns);
    }


    /**
     * Sets the character length
     *
     * @param integer
     * @return $this
     */
    public function length($value)
    {
        $this->where[] = "CHAR_LENGTH(`word`) = ?";
        $this->params[] = intval($value);
        return $this;

        /*
        $stmt = [];

        foreach ($ranges as $range) {
            list($from, $to) = $range;

            $from = (int) $from;
            $to = (int) $to;

            if ($from == 0 && $to > 0) {
                $stmt[] = "CHAR_LENGTH(`word`) <= ?";
                $this->params[] = $to;
            } else if ($from > 0 && $to == 0) {
                $stmt[] = "CHAR_LENGTH(`word`) >= ?";
                $this->params[] = $from;
            } else if ($from < $to && $from > 0) {
                $stmt[] = "CHAR_LENGTH(`word`) >= ? AND CHAR_LENGTH(`word`) <= ?";
                $this->params[] = $from;
                $this->params[] = $to;
            } else if ($from == $to && $from > 0) {
                $stmt[] = "CHAR_LENGTH(`word`) = ?";
                $this->params[] = $from;
            } else {
                throw new \Exception('Wrong length parameters.');
            }
        }
        
        $this->where[] = "(". implode(' OR ', $stmt) .")";

        // Returns current object
        return $this;
        */
    }

    /**
     * Sets word ordering
     *
     * @param string
     * @return $this
     */
    public function order($direction)
    {
        if ( ! is_string($direction)) {
            throw new \Exception('Invalid order direction.');
        }

        if (strtolower($direction) == 'rand') {
            $this->order = "RAND()";
        } else {
            $this->orderBy('word', $direction);
        }

        // Returns current object
        return $this;
    }


    /**
     * Sets the word limit (max - 100)
     *
     * @param integer
     * @return $this
     */
    public function take($limit)
    {
        if ( ! is_numeric($limit) || $limit <= 0) {
            throw new \Exception('Invalid limit number.');
        }

        // Sets word limit
        if($limit > 100) {
            $limit = 100;
        }

        return parent::take($limit);
    }


    /**
     * Disallows to delete Word models
     *
     */
    public function delete()
    {
        throw new \Exception('Forbidden to delete Word models.');
    }


    /**
     * Disallows to update Word models
     *
     */
    public function update(array $options)
    {
        throw new \Exception('Forbidden to update Word models.');
    }

}
