<?php

namespace Models;

use Services\Model;
use Contracts\AuthContract;

/**
 * User model
 *
 */
class User extends Model
{

    /**
     * Model table name
     * @var string
     */
    protected $table = 'users';


    /**
     * Set of user validation rules
     * @var array
     */
    public static $rules = [
        // Rules for user creating
        'create' => [
            'email' => ['required', 'email', 'unique:Models\User'],
            'name' => ['required'],
            'password' => ['required', 'min:6', 'confirm:password_confirm'],
        ]
    ];


    /**
     * Gets user by email address
     *
     * @param integer
     * @return Models\User
     */
    public function email($email)
    {
        return $this
            ->where('email', '=', $email);
    }

}
