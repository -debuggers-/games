<?php

namespace Models;

use Services\Model;
use Models\User;

/**
 * Password model
 *
 */
class PasswordReset extends Model
{

    /**
     * Model table name
     * @var string
     */
    protected $table = 'password_resets';


    /**
     * Password reset token length
     * @var integer
     */
    private static $tokenLength = 40;


    /**
     * Password reset token lifetime in seconds
     * @var integer
     */
    private static $tokenLifetime = 2400;


    /**
     * Creates new password reset token for email
     *
     * @param string
     * @return string|bool
     */
    public static function create($email)
    {
        // Destroys previous password reset token
        (new static)
            ->where('email', $email)
            ->delete();

        // Creates new password reset token
        $token = new static([
            'email' => $email,
            'token' => static::token(),
            'created_at' => date('Y-m-d H:i:s')
        ]);

        // Returns created token or false on failure
        return ($token->save())
            ? $token->token
            : false;
    }


    /**
     * Validates password token and return user model
     *
     * @param string
     * @param string
     * @return mixed
     */
    public static function getUser($token, $model)
    {
        $token = (new static)
            ->where('token', $token)
            ->first();

        // Checks if token and email matches
        if (is_null($token)) {
            return false;
        }

        // Checks if token expired
        if (static::expired($token)) {
            return false;
        }

        // Gets user by password reset email
        $user = (new $model)
            ->where('email', $token->email)
            ->first();

        return (is_null($user)) ? false : $user;
    }


    /**
     * Deletes token
     *
     * @param string
     * @return bool
     */
    public static function destroy($token)
    {
        return (new static)
            ->where('token', $token)
            ->delete();
    }


    /**
     * Checks if token already expired
     *
     * @param Models\PasswordReset
     * @return bool
     */
    private static function expired(self $token)
    {
        $expiry = strtotime($token->created_at) + static::$tokenLifetime;

        return $expiry < time();
    }


    /**
     * Generates random password reset token
     *
     * @return string
     */
    private static function token()
    {
        $token = bin2hex(openssl_random_pseudo_bytes(static::$tokenLength));
        return $token;  
    }

}
