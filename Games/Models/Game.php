<?php

namespace Models;

use Services\Model;

/**
 * Game model
 *
 */
class Game extends Model
{

    /**
     * Model table name
     * @var string
     */
    protected $table = 'games';

}
