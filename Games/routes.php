<?php

// every {name} MUST be unique
Services\Router::map([

    '/games/api/word' => 'Controllers\API\WordController@index',
    '/games/api/game' => 'Controllers\API\GameController@index',
    '/games/api/user' => 'Controllers\API\UserController@index',

    '/games/profile/edit' => [
        'GET' => 'Controllers\ProfileController@getEdit',
        'POST' => 'Controllers\ProfileController@postEdit'
    ],

    '/games/profile/forgot' => [
        'GET' => 'Controllers\ProfileController@getForgot',
        'POST' => 'Controllers\ProfileController@postForgot'
    ],

    '/games/profile/login' => [
        'GET' => 'Controllers\ProfileController@getLogin',
        'POST' => 'Controllers\ProfileController@postLogin'
    ],

    '/games/profile/signup' => [
        'GET' => 'Controllers\ProfileController@getSignup',
        'POST' => 'Controllers\ProfileController@postSignup'
    ],

    '/games/profile/reset/{token}' => [
        'GET' => 'Controllers\ProfileController@getReset',
        'POST' => 'Controllers\ProfileController@postReset'
    ],
    '/games/profile/reset' => 'Controllers\ProfileController@getReset',

    '/games/profile/logout' => 'Controllers\ProfileController@getLogout',
    '/games/profile/stats' => 'Controllers\ProfileController@getStats',
    '/games/profile' => 'Controllers\ProfileController@index',

    '/games/play/{game}' => 'Controllers\AppController@play',
    '/games/{query}' => 'Controllers\AppController@index',
    '/games' => 'Controllers\AppController@index',

]);
