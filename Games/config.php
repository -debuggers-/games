<?php

// Always log errors
ini_set('log_errors', 1);

// (do / do not) display errors
if ($_ENV['DEBUG'])
{
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}
else
{
    error_reporting(0);
}

// Apply necessary regional settings
ini_set('default_charset', 'UTF-8');
date_default_timezone_set('Europe/Riga');

// Define references to system's environmental constants
define('DB_HOST', $_ENV['DB_HOST']);
define('DB_NAME', $_ENV['DB_NAME']);
define('DB_USER', $_ENV['DB_USER']);
define('DB_PASSWORD', $_ENV['DB_PASSWORD']);
define('SITE_URL', $_ENV['SITE_URL']);

// Initialize services
Services\Session::init();
Services\Csrf::init();
