<?php

namespace Controllers;
use Services\Controller;

use Models\Game;

/**
 * Application Controller
 *
 */
class AppController extends Controller
{

    /**
     * GET: Index
     *
     */
    public function index($query = null)
    {
        if ( ! is_null($query)) {
            $this->abort(404);
        }        
        
        $this->view->title = 'Sākums';
        $this->view->games = (new Game)->get();

        // Renders home page
        return $this->view
            ->render('game/index');
    }


    /**
     * GET: Opens specific game in iframe
     *
     * @param string
     */
    public function play($name)
    {
        $game = (new Game)
            ->where('name', $name)
            ->first();

        // Checks if game exist
        if (is_null($game)) {
            $this->abort(404);
        }

        $this->view->title = $game->title;
        $this->view->url = '/games/'. $game->name;

        return $this->view
            ->render('game/play');
    }


    /**
     * Exits application with an error
     *
     * @param integer
     */
    protected function abort($code)
    {
        $this->view->title = (int) $code;

        // Renders error view depending on HTTP error code
        switch ($code) {
            case 404:
                header('HTTP/1.1 404 Not Found');
                echo $this->view->render('errors/404');
            break;
        }
        die;
    }

}
