<?php

namespace Controllers\API;
use Services\Controller;
use Controllers\API\ApiController;

/**
 * API Controller
 *
 */
class GameController extends ApiController
{

    public function index()
    {
        echo 'API/Game/index<br>';
        $vars = ['test1' => 'foo', 'test2' => 'bar'];
        $this->view
            ->render('game/test', $vars);
    }

}
