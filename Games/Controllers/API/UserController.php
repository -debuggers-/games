<?php

namespace Controllers\API;
use Services\Controller;

use Services\Auth;

/**
 * User Controller
 *
 */
class UserController extends ApiController
{

    public function index()
    {
        return $this->response(['user' => Auth::user()]);
    }

}
