<?php

namespace Controllers\API;

use Services\Controller;
use Services\Auth;

/**
 * API Controller
 *
 */
class ApiController extends Controller
{

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        if ( ! Auth::game($this->getApiKey())) {
            $this->unauthorized();
        }
    }


    /**
     * Sends JSON response
     *
     * @param array
     */
    protected function response(array $data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        die;
    }


    /**
     * Returns error response
     *
     * @param integer
     */
    private function unauthorized()
    {
        header('Content-Type: application/json');
        header("HTTP/1.1 401 Unauthorized");
        echo json_encode(['error' => 'Unauthorized']);
        die;
    }


    /**
     * Gets game API key from headers
     *
     * @return mixed
     */
    private function getApiKey()
    {
        // Gets all headers
        $headers = getallheaders();

        // Returns API key
        return (array_key_exists('X-Auth-Token', $headers))
            ? $headers['X-Auth-Token']
            : null;
    }

}
