<?php

namespace Controllers\API;
use Services\Controller;
use Controllers\API\ApiController;
use Models\Word;

/**
 * Word API Controller
 *
 */
class WordController extends ApiController
{

    /**
     * Allowed query parameters
     *
     */
    private $allowedParams = [
        'type', 'length', 'order', 'prefix', 'suffix', 'contains', 'take'
    ];


    /**
     * GET: Index
     *
     */
    public function index()
    {
        try {
            // Selects words
            $words = $this->selectWords($this->processQuery());
            
            $data = ['words' => []];
            
            foreach ($words as $word) {
                $data['words'][] = [
                    'word' => $word->word,
                    'type' => $word->type
                ];
            }

            // Generates data response
            return $this
                ->response($data);

        } catch (\Exception $e) {
            // Generates error response
            return $this
                ->response(['error' => $e->getMessage()]);
        }
    }


    /**
     * Process request query
     *
     * @return object
     */
    private function processQuery()
    {
        // Checks if GET parameter is not empty
        if (empty($_GET['q'])) {
            throw new \Exception('Empty API request.');
        }

        // Converts JSON string to PHP object
        $params = json_decode(urldecode($_GET['q']));

        // Checks if JSON format is correct
        if (json_last_error() != 0 || ! is_object($params)) {
            throw new \Exception('Invalid API request.');
        }

        // Returns params as object
        return $params;
    }


    /**
     * Selects words from database
     *
     * @param object 
     * @return array
     */
    private function selectWords($params)
    {
        $w = new Word;

        // Sets word parameters
        foreach ($params as $key => $val) {
            // Checks if parameter is allowed by Word API
            if ( ! in_array($key, $this->allowedParams)) {
                throw new \Exception('Invalid Word API parameter.');
            }
            // Sets parameter
            $w->$key($val);
        }

        // Gets all words
        return $w->get();
    }

}
