<?php

namespace Controllers;

use Services\Controller;
use Services\Request;
use Services\Auth;
use Services\Password;
use Services\Validator;
use Services\Session;
use Services\Csrf;
use Services\Mail;

use Models\User;
use Models\PasswordReset;

/**
 * Profile Controller
 *
 */
class ProfileController extends Controller
{

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * GET: Index
     *
     */
    public function index()
    {
        $this->onlyAuth();

        $this->view->title = 'Profils';
        $this->view->user = Auth::user();

        // Renders view
        return $this->view
            ->render('profile/index');
    }


    /**
     * GET: Login
     *
     */
    public function getLogin()
    {
        $this->onlyGuest();

        $this->view->title = 'Ielogoties';
        $this->view->csrf_token = Csrf::token();
        $this->view->error = Session::get('error');

        // Renders view
        return $this->view
            ->render('profile/login');
    }


    /**
     * POST: Login
     *
     */
    public function postLogin()
    {
        $this->onlyGuest();

        // Verifies CSRF token
        Csrf::verify($this->request->csrf_token);

        // Retrieves user credentials from POST request
        $email = $this->request->email;
        $password = $this->request->password;

        // Tries to login
        if (Auth::login($email, $password)) {
            // Redirects to profile page
            return $this->redirect
                ->to('profile');
        }

        // Sets error message
        Session::flash('error', 'Nepareizs e-pasts un/vai parole!');

        // Redirects back
        return $this->redirect
            ->to('profile/login');
    }


    /**
     * GET: Logout
     *
     */
    public function getLogout()
    {
        // Logs user out
        Auth::logout();

        // Redirect to the home page
        return $this->redirect
            ->home();
    }


    /**
     * GET: Sign up
     *
     */
    public function getSignup()
    {
        $this->onlyGuest();

        $this->view->title = 'Reģistrācija';
        $this->view->csrf_token = Csrf::token();        
        $this->view->errors = Session::get('errors');
        $this->view->input = Session::get('input');

        // Renders view
        return $this->view
            ->render('profile/signup');
    }


    /**
     * POST: Sign up
     *
     */
    public function postSignup()
    {
        $this->onlyGuest();

        // Verifies CSRF token
        Csrf::verify($this->request->csrf_token);

        // Creates validator object
        $v = new Validator($this->request->all(), User::$rules['create']);

        if ($v->failed()) {
            Session::flash('errors', $v->errors());
            Session::flash('input', $this->request->all());
            
            // Redirects back
            return $this->redirect
                ->to('profile/signup');
        }

        // Creates new user
        $user = new User([
            'email' => $this->request->email,
            'name' => filter_var($this->request->name, FILTER_SANITIZE_STRING),
            'password' => Password::hash($this->request->password)
        ]);

        // Saves user
        if ($user->save()) {
            Session::flash('ok', true);
            // Redirects back
            return $this->redirect
                ->to('profile/signup');
        }
    }


    /**
     * GET: Edit
     *
     */
    public function getEdit()
    {
        return 'Profile/edit [GET]<br>';
    }


    /**
     * POST: Edit
     *
     */
    public function postEdit()
    {
        return 'Profile/edit [POST]<br>';
    }


    /**
     * GET: Forgot
     *
     */
    public function getForgot()
    {
        $this->onlyGuest();

        $this->view->title = 'Aizmirsu paroli';
        $this->view->csrf_token = Csrf::token();        
        $this->view->error = Session::get('error');

        // Renders view
        return $this->view
            ->render('profile/forgot');
    }


    /**
     * POST: Forgot
     *
     */
    public function postForgot()
    {
        $this->onlyGuest();

        // Verifies CSRF token
        Csrf::verify($this->request->csrf_token);

        $email = $this->request->email;
        
        // Tries to find user by email address
        $user = (new User)
            ->where('email', $email)
            ->first();

        // Checks if user exists
        if (is_null($user)) {
            Session::flash('error', 'Lietotājs ar šādu e-pasta adresi nav reģistrēts!');
        } else {
            // Creates new password reset token
            $token = PasswordReset::create($email);
            $link = SITE_URL . 'profile/reset/' . $token;

            $message = $this->view
            	->layout('shared/email')
                ->render('email/forgot', ['link' => $link]);
            
            // Sends reset link to the user's email address
            Mail::send($message, $user->email, 'Paroles maiņa');
            Session::flash('ok', true);
        }

        return $this->redirect
            ->to('profile/forgot');
    }


    /**
     * GET: Reset
     *
     * @param string
     */
    public function getReset($token = null)
    {
        $this->onlyGuest();

        // Validates request
        if ( ! PasswordReset::getUser($token, User::class) && ! Session::has('ok')) {
            return $this->redirect
                ->home();
        }

        $this->view->title = 'Atjaunot paroli';
        $this->view->csrf_token = Csrf::token();        
        $this->view->error = Session::get('error');

        // Renders view
        return $this->view
            ->render('profile/reset');
    }


    /**
     * POST: Reset
     *
     * @param string
     */
    public function postReset($token)
    {
        $this->onlyGuest();

        // Verifies CSRF token
        Csrf::verify($this->request->csrf_token);

        // Gets POST input
        $input = $this->request->all();

        // Validates token and gets user
        $user = PasswordReset::getUser($token, User::class);

        if ($user) {
            if (Password::validate($input['password'], $input['password_confirm'])) {
                // Deletes token
                PasswordReset::destroy($token);

                // Updates user password
                $user->update([
                    'password' => Password::hash($input['password'])
                ]);

                // Sets success status
                Session::flash('ok', true);
                return $this->redirect
                    ->to('profile/reset');
            } else {
                Session::flash('error', 'Parolēm jāsakrīt un minimālais simbolu skaits ir 6!');
            }
        } else {
            Session::flash('error', 'Paroles atjaunošanas pieprasījums nav derīgs!');            
        }    

        return $this->redirect
            ->to('profile/reset/'.$token);
    }


    /**
     * GET: Statistics
     *
     */
    public function getStats()
    {
        return 'Profile/stats [GET]<br>';
    }


    /**
     * GET: Game statistics
     *
     */
    public function getGameStats($param)
    {
        return "Profile/gameStats for {$param[0]} [GET]<br>";
    }


    /**
     * Route filter: only for authenticated users
     *
     */
    private function onlyAuth()
    {
        if ( ! Auth::check() ) {
            return $this->redirect->to('profile/login');
        }
    }


    /**
     * Route filter: only for guests
     *
     */
    private function onlyGuest()
    {
        if (Auth::check()) {
            return $this->redirect->home();
        }
    }

}
